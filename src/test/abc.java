package test;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JApplet;
import javax.swing.Timer;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.data.time.*;

/**
 * 一个简单的曲线图例子
 *
 * @author tf
 * date 2007-01-30
 */

public class abc extends JApplet {
    private static final long serialVersionUID = 3257566209007171634L;

    private double cpuValue1;
    private double cpuValue2;
    private double cpuValue3;


    private TimeSeries cpu1;
    private TimeSeries cpu2;
    private TimeSeries cpu3;

    /**
     * 内部类--实现动态随机产生虚拟CPU使用率的值
     *
     * @author tf
     */
    class DataGenerator extends Timer implements ActionListener {
        private static final long serialVersionUID = 3977867288743720505L;

        public void actionPerformed(ActionEvent actionevent) {
            cpuValue1 = 100;
            cpuValue2 = 100;
            cpuValue3 = 100;

            double d1 = Math.random();
            cpuValue1 = cpuValue1 * d1;
            //cpuValue1 += 10;

            double d2 = Math.random();
            cpuValue2 = cpuValue2 * d2;

            double d3 = Math.random();
            cpuValue3 = cpuValue3 * d3;

            addcpu1Observation(cpuValue1);
            addcpu2Observation(cpuValue2);
            addcpu3Observation(cpuValue3);
        }

        DataGenerator() {
            super(100, null);
            addActionListener(this);
        }
    }

    public abc() {
        cpu1 = new TimeSeries("处理器1", org.jfree.data.time.Millisecond.class);//增加一条走势曲线
        //cpu1.setHistoryCount(2000); //---比方法在新版本中的TimeSeries类中不存在
        cpu2 = new TimeSeries("处理器2", Millisecond.class);
        //free.setHistoryCount(2000); //---比方法在新版本中的TimeSeries类中不存在
        cpu3 = new TimeSeries("处理器3", Millisecond.class);

        TimeSeriesCollection timeseriescollection = new TimeSeriesCollection();
        //将所有走势曲线加入到时间条中
        timeseriescollection.addSeries(cpu1);
        timeseriescollection.addSeries(cpu2);
        timeseriescollection.addSeries(cpu3);

        //创建JFreeChart对象
        JFreeChart jfreechart = ChartFactory.createTimeSeriesChart("处理器利用率",
                "时间", "值", timeseriescollection, true, true, true);

        //设置图表样式
        XYPlot xyplot = jfreechart.getXYPlot();
        xyplot.setOutlinePaint(Color.CYAN);//设置数据区的边界线条颜色
        ValueAxis valueaxis = xyplot.getDomainAxis();
//  valueaxis.setAutoRange(true);      //自动设置数据轴数据范围
        valueaxis.setFixedAutoRange(10000D);//设置时间轴显示的数据
        valueaxis = xyplot.getRangeAxis();
        valueaxis.setRange(0.0D, 100D);//数据轴固定数据范围（设置100的话就是显示MAXVALUE到MAXVALUE-100那段数据范围）
//  valueaxis.setAutoRange(true);
        valueaxis.setVisible(true);//设置是否显示数据轴

        //设置曲线图面版
        ChartPanel chartpanel = new ChartPanel(jfreechart);
        chartpanel.setPopupMenu(null);
        ChartPanel chartpanel1 = new ChartPanel(jfreechart);
        chartpanel1.setPopupMenu(null);
        getContentPane().add(chartpanel);
        getContentPane().add(chartpanel1);
        (new DataGenerator()).start();
    }

    private void addcpu1Observation(double d) {

        cpu1.add(new Millisecond(), d);
    }

    private void addcpu2Observation(double d) {
        cpu2.add(new Millisecond(), d);
    }

    private void addcpu3Observation(double d) {
        cpu3.add(new Millisecond(), d);
    }
    public static void main(String args){
        abc obj = new abc();
    }
        

}